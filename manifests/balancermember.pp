define load_balancer_haproxy::balancermember (
  $balancermember_name = $title,
  $listening_service,
  $server_names,
  $ipaddresses,
  $ports,
  $options = undef,
) {
  include load_balancer_haproxy::params

  if ($options) {
    $_options = $options
  }

  else {
    $_options = $load_balancer_haproxy::params::balancermember_options
  }

  haproxy::balancermember { $balancermember_name:
    listening_service => $listening_service,
    server_names      => $server_names,
    ipaddresses       => $ipaddresses,
    ports             => $ports,
    options           => $options,
  }
}