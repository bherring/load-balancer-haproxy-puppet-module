class load_balancer_haproxy::params {
  $user = 'haproxy'
  $group  = 'haproxy'

  $conf_dir = '/etc/haproxy'

  # Server
  $global_options = {
      'log'     => "127.0.0.1 local2",
      'chroot'  => '/var/lib/haproxy',
      'pidfile' => '/var/run/haproxy.pid',
      'maxconn' => '4000',
      'user'    => $user,
      'group'   => $group,
      'daemon'  => '',
      'stats'   => 'socket /var/lib/haproxy/stats',
  }

  $default_options = {
      'log'     => 'global',
      'stats'   => 'enable',
      'option'  => [
        'redispatch',
      ],
      'retries' => '3',
      'timeout' => [
        'http-request 10s',
        'queue 1m',
        'connect 10s',
        'client 1m',
        'server 1m',
        'check 10s',
      ],
      'maxconn' => '8000',
  }

  # Frontend
  $frontend_options = {
    'timeout client'  => '30s',
    'option'          => [
      'tcplog',
      'accept-invalid-http-request',
    ],
  }

  $frontend_bind_options = ''
  $frontend_mode = 'tcp'

  # Backend
  $backend_options = {
    'option'  => [
      'tcplog',
    ],
    'balance' => 'roundrobin',
  }

  # Balancer Member
  $balancermember_options = 'check'

  # Mapfile
  $mapfile_owner  = 'root'
  $mapfile_group  = 'root'
  $mapfile_mode   = '644'
}