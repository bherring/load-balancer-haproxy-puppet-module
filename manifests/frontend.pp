define load_balancer_haproxy::frontend (
  $frontend_name = $title,
  $ports,
  $default_backend = undef,
  $options = undef,
  $bind_options = undef,
  $mode = undef,
  $ipaddress = undef,
) {
  include load_balancer_haproxy::params

  if ($options) {
    $_options = $options
  }

  else {
    if ($default_backend) {
      $_options = merge($load_balancer_haproxy::params::frontend_options, {'default_backend' => $default_backend})
    }

    else {
      $_options = $load_balancer_haproxy::params::frontend_options
    }
  }

  if ($bind_options) {
    $_bind_options = $bind_options
  }

  else {
    $_bind_options = $load_balancer_haproxy::params::frontend_bind_options
  }

  if ($mode) {
    $_mode = $mode
  }

  else {
    $_mode = $load_balancer_haproxy::params::frontend_mode
  }

  if ($ipaddress) {
    $_ipaddress = $ipaddress
  }

  else {
    $_ipaddress = $::ipaddress
  }

  haproxy::frontend { $frontend_name:
	  ipaddress     => $_ipaddress,
	  ports         => $ports,
	  mode          => $_mode,
	  bind_options  => $_bind_options,
	  options       => $_options,
	}
}