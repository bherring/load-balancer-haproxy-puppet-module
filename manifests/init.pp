class load_balancer_haproxy (
  $global_options  = $load_balancer_haproxy::params::global_options,
  $default_options = $load_balancer_haproxy::params::default_options) inherits load_balancer_haproxy::params {
  class { 'load_balancer_haproxy::setup':
    global_options  => $global_options,
    default_options => $default_options,
  }
}