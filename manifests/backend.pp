define load_balancer_haproxy::backend (
  $backend_name = $title,
  $options      = undef,) {
  include load_balancer_haproxy::params

  if ($options) {
    $_options = $options
  } else {
    $_options = $load_balancer_haproxy::params::backend_options
  }

  haproxy::backend {
    $backend_name: options => $_options,
  }
}