define load_balancer_haproxy::mapfile::mapping (
  $hosts,
  $mapfile_name,
  $backend,
  $order = '010') {
  include load_balancer_haproxy::params

  concat::fragment { "${load_balancer_haproxy::params::conf_dir}/${mapfile_name}.map_${title}_${hosts}_${backend}":
    target  => "${load_balancer_haproxy::params::conf_dir}/${mapfile_name}.map",
    content => template('load_balancer_haproxy/mapfile/mapping.erb'),
    order   => $order,
  }
}