class load_balancer_haproxy::setup (
  $global_options,
  $default_options) inherits load_balancer_haproxy::params {
  class { 'haproxy':
    global_options   => $global_options,
    defaults_options => $default_options,
  }
}