define load_balancer_haproxy::mapfile (
  $file_name = $title,
  $ensure    = present,
  $owner     = undef,
  $group     = undef,
  $mode      = undef) {
  include load_balancer_haproxy::params

  if ($owner) {
    $_owner = $owner
  } else {
    $_owner = $load_balancer_haproxy::params::mapfile_owner
  }

  if ($group) {
    $_group = $group
  } else {
    $_group = $load_balancer_haproxy::params::mapfile_group
  }

  if ($mode) {
    $_mode = $mode
  } else {
    $_mode = $load_balancer_haproxy::params::mapfile_mode
  }

  concat { "${load_balancer_haproxy::params::conf_dir}/${file_name}.map":
    ensure         => $ensure,
    owner          => $_owner,
    group          => $_group,
    mode           => $_mode,
    ensure_newline => true,
  }

  concat::fragment { "${load_balancer_haproxy::params::conf_dir}/${file_name}.map_header":
    target  => "${load_balancer_haproxy::params::conf_dir}/${file_name}.map",
    content => template('util/header.erb'),
    order   => '001'
  }
}